package com.homeprojects.springdatajpa.repository;

import com.homeprojects.springdatajpa.entity.Course;
import com.homeprojects.springdatajpa.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TeacherRepositoryTest {

    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    public void saveTeacher() {
        Course course = Course.builder()
                .title("DBA")
                .credit(5)
                .build();

        Course courseDba = Course.builder()
                .title("Lol")
                .credit(3)
                .build();

        Teacher teacher = Teacher.builder()
                .firstName("Tomek")
                .lastName("Strycharz")
//                .course(List.of(course, courseDba))
                .build();

        teacherRepository.save(teacher);

        System.out.println("teacher = " + teacher);
    }

}