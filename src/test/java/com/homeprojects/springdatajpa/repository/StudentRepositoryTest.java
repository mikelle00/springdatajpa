package com.homeprojects.springdatajpa.repository;

import com.homeprojects.springdatajpa.entity.Guardian;
import com.homeprojects.springdatajpa.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent() {
        Student student = Student.builder()
                .emailId("marcin@gmail.com")
                .firstName("Marcin")
                .lastName("Elek")
                //.guardianName("Tomasz")
                //.guardianEmail("tomasz@gmail.com")
                //.guardianMobile("500500500")
                .build();

        studentRepository.save(student);
    }

    @Test
    public void saveStudentWithGuardian() {
        Guardian guardian = Guardian.builder()
                .email("tomasz@gmail.com")
                .name("Tomasz")
                .mobile("500500500")
                .build();

        Student student = Student.builder()
                .firstName("Filip")
                .emailId("filip@gmail.com")
                .lastName("Trem")
                .guardian(guardian)
                .build();

        studentRepository.save(student);
    }

    @Test
    public void printAllStudents() {
        List<Student> studentList = studentRepository.findAll();

        System.out.println("StudentList: " + studentList);
    }

    @Test
    public void printStudentByFirstName() {
        List<Student> students = studentRepository.findByFirstName("Marcin");

        System.out.println("students: " + students);
    }

    @Test
    public void printStudentByFirstNameContaining() {
        List<Student> students = studentRepository.findByFirstNameContaining("Mar");

        System.out.println("students: " + students);
    }

    @Test
    public void printStudentBasedOnGuardianName() {
        List<Student> students = studentRepository.findByGuardianName("Tomasz");

        System.out.println("students = " + students);
    }

    @Test
    public void printStudentByEmailAddress() {
        Student student = studentRepository.getStudentByEmailAddress("filip@gmail.com");

        System.out.println("student = " + student);
    }

    @Test
    public void printStudentFirstNameByEmailAddress() {
        String firstName = studentRepository.getStudentFirstNameByEmailAddress("filip@gmail.com");

        System.out.println("firstName = " + firstName);
    }


    @Test
    public void printStudentFirstNameByEmailAddressNative() {
        Student student = studentRepository.getStudentByEmailAddressNative("filip@gmail.com");

        System.out.println("student = " + student);
    }

    @Test
    public void printStudentFirstNameByEmailAddressNativeNamedParam() {
        Student student = studentRepository.getStudentByEmailAddressNativeNamedParam("filip@gmail.com");

        System.out.println("student = " + student);
    }

    @Test
    public void updateStudentNameByEmailIdTest() {
        studentRepository.updateStudentNameByEmailId("Arek", "marcin@gmail.com");
    }
}