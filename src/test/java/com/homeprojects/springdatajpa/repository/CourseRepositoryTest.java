package com.homeprojects.springdatajpa.repository;

import com.homeprojects.springdatajpa.entity.Course;
import com.homeprojects.springdatajpa.entity.Student;
import com.homeprojects.springdatajpa.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void printCourses() {
        List<Course> courses = courseRepository.findAll();

        System.out.println("courses = " + courses);
    }

    @Test
    public void saveCourseWithTeacher() {
        Teacher teacher = Teacher.builder()
                .firstName("Alan")
                .lastName("Walker")
                .build();

        Course course = Course.builder()
                .title("Java course fe")
                .credit(2)
                .teacher(teacher)
                .build();

        courseRepository.save(course);
    }

    @Test
    public void findAllPagination() {
        Pageable firstPageWithThreeRecords = (Pageable) PageRequest.of(0, 3);
        Pageable secondPageWithThreeRecords = (Pageable) PageRequest.of(1, 2);

        List<Course> courses = courseRepository.findAll((org.springframework.data.domain.Pageable) firstPageWithThreeRecords).getContent();

        long totalElements = courseRepository.findAll((org.springframework.data.domain.Pageable) firstPageWithThreeRecords).getTotalElements();
        long totalPages = courseRepository.findAll((org.springframework.data.domain.Pageable) firstPageWithThreeRecords).getTotalPages();


        System.out.println("courses = " + courses);

        System.out.println("totalPages = " + totalPages);
        System.out.println("totalElements = " + totalElements);
    }

    @Test
    public void findAllSorting() {
        Pageable sortByTitle = PageRequest.of(0, 2, Sort.by("title"));
        Pageable sortByCreditDsc = PageRequest.of(0, 2, Sort.by("credit").descending());
        Pageable sortByTitleAndCreditDsc = PageRequest.of(0, 2, Sort.by("title").descending().and(Sort.by("credit")));

        List<Course> courses = courseRepository.findAll(sortByTitle).getContent();

        System.out.println("courses = " + courses);
    }

    @Test
    public void findByTitleContaining() {
        Pageable firstPageTenRecords = PageRequest.of(0, 10);

        List<Course> courses = courseRepository.findByTitleContaining("D", firstPageTenRecords).getContent();

        System.out.println("courses = " + courses);
    }

    @Test
    public void saveCourseWithStudentAndTeacher() {
        Teacher teacher = Teacher.builder()
                .firstName("Arek")
                .lastName("Loka")
                .build();

        Student student = Student.builder()
                .firstName("Marcin")
                .lastName("Awd")
                .emailId("maarcin@gmail.com")
                .build();

        Course course = Course.builder()
                .title("CS")
                .credit(10)
                .teacher(teacher)
                .build();

        course.addStudents(student);
        courseRepository.save(course);

        System.out.println("course = " + course);
    }
}